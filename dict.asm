global find_word
extern string_equals

section .text
find_word:
	push rdi
	push rsi
	mov rsi, [rsi + 8]
	call string_equals
	pop rsi
	pop rdi
	test rax, rax
	jne .end
	mov rsi, [rsi]
	test rsi, rsi
	jne find_word
	
.end:
	mov rax, rsi
	ret
