ASM=nasm
ASMFLAGS=-f elf64
LD=ld

.DEFAULT_GOAL: main

main: dict.o lib.o main.o 
	$(LD) -o $@ $^

main.o: main.asm colon.inc lib.inc words.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<