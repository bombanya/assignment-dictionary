%define BUFFER_SIZE 256

section .data

%include 'words.inc'

no_such_key_msg: db 'there is no such key in the dictionary', 10, 0
input_err_msg: db 'error on input', 10, 0

global _start
%include 'lib.inc'
extern find_word

section .text
_start:
	sub rsp, BUFFER_SIZE
	mov rdi, rsp
	mov rsi, BUFFER_SIZE
	call read_string
	test rax, rax
	je .input_err
	mov rdi, rsp
	mov rsi, node
	call find_word
	add rsp, BUFFER_SIZE
	test rax, rax
	je .no_such_key
	mov rdi, [rax + 16]
	call print_string
	call print_newline
	xor rdi, rdi
	call exit	
	
.input_err:
	add rsp, BUFFER_SIZE
	mov rdi, input_err_msg
	jmp .error_exit
	
.no_such_key:
	mov rdi, no_such_key_msg
.error_exit:
	call print_error
	mov rdi, 1
	call exit
	