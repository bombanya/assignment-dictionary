%define node 0

%macro colon 2
	%ifstr %1
		%ifid %2
			%%some_label: dq node, .key, %2
			.key: db %1, 0
			%2:
			
			%define node %%some_label
		%else
			%error "the second argument must be a label"
		%endif
	%else
		%error "the first argument must be a string"
	%endif
%endmacro
