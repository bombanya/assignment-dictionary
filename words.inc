%include 'colon.inc'

colon "first key", first
db "first value", 0

colon "%1mposible!", impossible
db "\(^-^)/", 0

colon "spider", spider
db "//\(oo)/\\", 0

colon "spuceinvader", spuceinvader
db "─=≡Σ(╯°□°)╯︵┻┻", 0

colon "third word", third_word
db "third word explanation", 0


;colon test, test
;db "error test", 0

;colon 'test', !test
;db "error test", 0